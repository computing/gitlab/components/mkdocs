# `mkdocs/pages`

Configures a job that publishes the output of an MkDocs build with
[Gitlab Pages](https://git.ligo.org/help/user/project/pages/).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/mkdocs/pages@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `"deploy"` | Pipeline stage to add jobs to (must be configured in `stages`) |
| `needs` | `"mkdocs"` {: .nowrap } | The name of the mkdocs build job to publish |
| `pages_when` {: .nowrap } | `"never"` | When to automatically publish the documentation using Gitlab Pages, one of `"tags"` (for all git tags), `"default"` (pushes to the project default branch) |
| `site_dir` | `"site"` | Path of the site directory written by mkdocs build |

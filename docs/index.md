# MkDocs CI/CD components

The `computing/gitlab/components/mkdocs` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for an [MkDocs](https://www.mkdocs.org/) project.

## Latest release

[![latest badge](https://git.ligo.org/computing/gitlab/components/mkdocs/-/badges/release.svg?text=test)](https://git.ligo.org/explore/catalog/computing/gitlab/components/mkdocs/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`mkdocs/build`](./build.md "`mkdocs/build` component documentation")
- [`mkdocs/pages`](./pages.md "`mkdocs/pages` component documentation")

# `mkdocs/build`

Configure jobs to build and publish a website for this project using
[MkDocs](https://www.mkdocs.org).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/mkdocs/build@<VERSION>
    inputs:
      stage: build
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `"mkdocs"` | Name to give the build job |
| `image` | [`python`](https://hub.docker.com/_/python) | Container image in which to build the documentation |
| `stage` | `"test"` | Pipeline stage to add jobs to |
| `project_dir` | `"."` | Directory containing the `mkdocs.yml` file |
| `requirements` | `""` | Extra requirements to install with `pip` [Tip: pass `requirements: -r requirements.txt` to install from a requirements file] |
| `mkdocs_options` {: .nowrap} | `"--clean --verbose"` {: .nowrap } | Options to pass to mkdocs build |
| `pages_when` | `"never"` | When to automatically publish the documentation using Gitlab Pages, one of `"tags"` (for all git tags), `"default"` (pushes to the project default branch), or `"never"` (don't publish anything) |

## Examples

### Automatically deploy to gitlab pages { #pages }

A common use case is to build a website in all pipelines (including
development and testing) and automatically deploy it from the default
branch (`main` or `master`) using
[GitLab Pages](https://git.ligo.org/help/user/project/pages/index.html).

!!! example "Automatically deploy to gitlab pages for tags"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/mkdocs/build@<VERSION>
        inputs:
          stage: build
          pages_when: default
    ```

See the [`mkdocs/pages`](./pages.md) component documentation for more details.
For more control over the pages deployment, the `mkdocs/pages` component can
be `include`d manually and configured using its `inputs`.
